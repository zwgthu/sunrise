/*
    Copyright 2006-2010 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Declaration of the aux_emergence class.

// $Id$

#ifndef __aux_emergence__
#define __aux_emergence__

#include "emergence.h"

#include "aux_grid.h"

namespace mcrx {
  class aux_grid_emergence;
  class aux_particle_emergence;
  void write_aux_keywords(CCfits::HDU*, const T_unit_map&, T_float);
}

class binofstream;
class binifstream;

/** Derivation of the emergence class for collecting auxiliary grid
    quantities.  This is used for making images of gas mass and star
    formation rate, etc.  */
class mcrx::aux_grid_emergence: public emergence<aux_pars_type> {
public:
  // Constructor takes all the imaging parameters
  aux_grid_emergence(int nt, int np, T_float cd, T_float fov,
		unsigned int size, bool exclude_south_pole = false):
    emergence<T_image> (nt, np, cd, fov, size, exclude_south_pole) {};
  aux_grid_emergence(const std::vector<std::pair<T_float, T_float> >& cam_pos,
		T_float cd, T_float fov, unsigned int size):
    emergence<T_image> (cam_pos, cd, fov, size) {};
  aux_grid_emergence (const CCfits::FITS& file):
    emergence<T_image> (file) {};


  // write camera content to FITS files
  void write_images (CCfits::FITS&, T_float normalization,
		     const T_unit_map& units);
  void load_images (CCfits::FITS& file);
  void write_dump (binofstream& file) const;
  bool load_dump (binifstream& file);
};

/** Derivation of the emergence class for collecting auxiliary
    particle quantities.  This is used for making images of stellar
    mass and luminosity, etc.  */
class mcrx::aux_particle_emergence: public emergence<aux_pars_type> {
public:
  // Constructor takes all the imaging parameters
  aux_particle_emergence(int nt, int np, T_float cd, T_float fov,
		unsigned int size, bool exclude_south_pole = false):
    emergence<T_image> (nt, np, cd, fov, size, exclude_south_pole) {};
  aux_particle_emergence(const std::vector<std::pair<T_float, T_float> >& cam_pos,
		T_float cd, T_float fov, unsigned int size):
    emergence<T_image> (cam_pos, cd, fov, size) {};
  aux_particle_emergence (const CCfits::FITS& file):
    emergence<T_image> (file) {};


  // write camera content to FITS files
  void write_images (CCfits::FITS&, T_float normalization,
		     const T_unit_map& units);
  void load_images (CCfits::FITS& file);
  void write_dump (binofstream& file) const;
  bool load_dump (binifstream& file);
};

#endif

