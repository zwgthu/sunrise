/*
    Copyright 2010-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Implementation of monitor class.

// $Id$

#include "monitor.h"

#ifdef WITH_X11


#include <boost/thread/thread.hpp>
#include "boost/date_time/posix_time/posix_time_types.hpp"
#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glu.h>

using namespace boost;
using namespace blitz;

extern "C" {
  /* This is necessary because c++ can't handle the struct member called
     class. */
  int* getXVisualInfo_class(XVisualInfo* xv);
};

/** Sleep the thread for a short interval. */
void mcrx::monitor::sleep()
{
  posix_time::seconds t(5);
  this_thread::sleep(t);
}

/** Wait for modified_ to change. */
void mcrx::monitor::wait_for_data()
{
  glClearColor(0.1,0.1,0.1,0.);
  glClear(GL_COLOR_BUFFER_BIT);
  glFlush();

  while(true) {
    // bools are atomic so we can look at it without locking
    if(modified_) {
      modified_=false;
      if(!data_.empty())
      break;
    }
    sleep();
  }
}
  
void mcrx::monitor::operator() ()
{
  setup_window();

  int i;
  while(true) {
    wait_for_data();
    i=0;

    // until the data are modified again, we loop
    while(!modified_ && !data_.empty()) {

      // We just ignore X Events
      XEvent event;
      while (XPending(dpy_)) {
	XNextEvent(dpy_, &event);
      } 

      // lock and get image data
      Array<float,2> pixels;
      {
	mutex::scoped_lock l (m_);
	if(modified_)
	  break;

	// get pixel data
	array_3 current_image;
	current_image.weakReference(*data_[i]);
	pixels.resize(current_image.extent(firstDim), current_image.extent(secondDim));
	pixels=current_image(Range::all(), Range::all(), ref_)/
	  max(current_image(Range::all(), Range::all(), ref_));

	++i;
	if(i==data_.size())
	  i=0;
      }
      
      draw_image(pixels);

      sleep();
    }
  }
}

/** Draw the image in pixels. */
void mcrx::monitor::draw_image(Array<float,2>& pixels)
{
  const float invlogmin=1./log(1e-4);
  pixels=1 - invlogmin * where(pixels>0, log(pixels), 1./invlogmin);

  glClearColor(0.1,0.1,0.1,0.);
  glClear(GL_COLOR_BUFFER_BIT);
  glRasterPos2i(0,0);
  glDrawPixels(pixels.columns(), pixels.rows(), GL_LUMINANCE, GL_FLOAT, pixels.data());
  glFlush();
}

/** Set up the X11 window. */
void mcrx::monitor::setup_window()
{
  cout << "Setting up X11 window" << endl;

  XVisualInfo *vi;
  Colormap cmap;
  XSetWindowAttributes swa;
  GLXContext cx;
  XEvent event;
  Bool needRedraw = False, recalcModelView = True;
  int dummy;

  static int snglBuf[] = {GLX_RGBA, GLX_RED_SIZE, 1, GLX_GREEN_SIZE, 1,
			  GLX_BLUE_SIZE, 1, None};

  dpy_ = XOpenDisplay(NULL);
  if (dpy_ == NULL)
    assert(0);

  if (!glXQueryExtension(dpy_, &dummy, &dummy))
    assert(0);

  vi = glXChooseVisual(dpy_, DefaultScreen(dpy_), snglBuf);
  if (vi == NULL)
    assert(0);

  int cl=*getXVisualInfo_class(vi);
  if (cl != TrueColor)
    ;//assert(0);

  cx = glXCreateContext(dpy_, vi,
    /* No sharing of display lists */ None,
    /* Direct rendering if possible */ True);
  if (cx == NULL)
    assert(0);

  cmap = XCreateColormap(dpy_, RootWindow(dpy_, vi->screen), vi->visual, AllocNone);
  swa.colormap = cmap;
  swa.border_pixel = 0;
  swa.event_mask = ExposureMask | ButtonPressMask | StructureNotifyMask;
  win_ = XCreateWindow(dpy_, RootWindow(dpy_, vi->screen), 0, 0, 300, 300, 0, vi->depth,
    InputOutput, vi->visual, CWBorderPixel | CWColormap | CWEventMask, &swa);
  XSetStandardProperties(dpy_, win_, "Sunrise monitor", "Sunrise monitor", None, 0, 0, NULL);

  glXMakeCurrent(dpy_, win_, cx);
  glDisable(GL_DEPTH_TEST);
  gluOrtho2D(0,1,0,1);

  XMapWindow(dpy_, win_);

  // free memory?
}

#endif
